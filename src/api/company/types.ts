/**
 * 公司查询参数
 */
export interface CompanyQuery extends PageQuery {
  keywords?: string
  status?: number
}

/**
 * 公司表单类型
 */
export interface CompanyForm {
  /**
   * 公司ID(新增不填)
   */
  id?: number
  /**
   * 公司名称
   */
  name?: string
  /**
   * 注册地
   */
  domicile?: string
  /**
   * 资质管理同事
   */
  manager?: string
  /**
   * 是否为国家高新技术企业
   */
  isNHTE?: boolean
  /**
   * 国高证书编号
   */
  nhteNumber?: string
  /**
   * 是否为专精特新企业
   */
  isSPSEnterprises?: boolean
  /**
   * 最新认定成功时间
   */
  spsAppliTime?: string
  /**
   * 是否为创新型中小企业
   */
  isInnovativeSME?: boolean
  /**
   * 最新认定成功时间
   */
  smeAppliTime?: string
  /**
   * 是否为科技型中小企业
   */
  isSMETech?: boolean
  /**
   * 最新认定成功时间
   */
  smeTechAppliTime?: string
  /**
   * 排序
   */
  sort?: number
  /**
   * 状态(1:启用；0：禁用)
   */
  status?: number
}

/**
 * 公司类型
 */
export interface CompanyVO extends CompanyForm {
  /**
   * 创建时间
   */
  createTime?: Date
  /**
   * 修改时间
   */
  updateTime?: Date
}

/**
 * 分页项类型声明
 */
export type CompanyTypePageResult = PageResult<CompanyVO[]>
