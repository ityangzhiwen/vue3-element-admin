import request from '@/utils/request'
import { AxiosPromise } from 'axios'
import {
  CompanyForm,
  CompanyQuery,
  CompanyVO,
  CompanyTypePageResult
} from './types'

/**
 * 部门列表
 *
 * @param queryParams
 */
export function listComapny(
  queryParams?: CompanyQuery
): AxiosPromise<CompanyTypePageResult> {
  return request({
    url: '/api/v1/company',
    method: 'get',
    params: queryParams
  })
}

/**
 * 部门列表-不分页
 *
 * @param queryParams
 */
export function listComapnys(): AxiosPromise<CompanyTypePageResult> {
  return request({
    url: '/api/v1/companys',
    method: 'get',
    params: queryParams
  })
}

/**
 * 获取公司详情
 *
 * @param id
 */
export function getCompanyForm(id: number): AxiosPromise<CompanyForm> {
  return request({
    url: '/api/v1/company/' + id + '/form',
    method: 'get'
  })
}

/**
 * 新增公司
 *
 * @param data
 */
export function addCompany(data: CompanyForm) {
  return request({
    url: '/api/v1/company',
    method: 'post',
    data: data
  })
}

/**
 *  修改公司
 *
 * @param id
 * @param data
 */
export function updateCompany(id: number, data: CompanyForm) {
  return request({
    url: '/api/v1/company/' + id,
    method: 'put',
    data: data
  })
}

/**
 * 删除公司
 *
 * @param ids
 */
export function deleteCompany(ids: string) {
  return request({
    url: '/api/v1/company/' + ids,
    method: 'delete'
  })
}
