const defaultSettings: AppSettings = {
  title: '华测资质平台',
  version: 'v2.9.0',
  showSettings: true,
  tagsView: true,
  fixedHeader: false,
  sidebarLogo: true,
  layout: 'left',
  theme: 'light',
  size: 'default',
  language: 'zh-cn',
  themeColor: '#409EFF',
  watermarkEnabled: false,
  watermarkContent: '华测资质平台'
}

export default defaultSettings
