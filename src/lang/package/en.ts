export default {
  // 路由国际化
  route: {
    dashboard: 'Dashboard',
    document: 'Document'
  },
  // 登录页面国际化
  login: {
    username: 'Username',
    password: 'Password',
    login: 'Login',
    captchaCode: 'Verify Code'
  },
  // 导航栏国际化
  navbar: {
    dashboard: 'Dashboard',
    logout: 'Logout',
    document: 'Document',
    gitee: 'Gitee'
  },
  option: {
    enable: 'Enable',
    disabled: 'Disabled'
  },
  rules: {
    emailTip: 'Please enter the correct email address',
    mobileTip: 'Please enter the correct phone number',
    usernameTip: 'The username cannot be empty'
  }
}
