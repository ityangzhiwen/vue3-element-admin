export default {
  // 路由国际化
  route: {
    dashboard: '首页',
    document: '项目文档'
  },
  // 登录页面国际化
  login: {
    username: '用户名',
    password: '密码',
    login: '登 录',
    captchaCode: '验证码'
  },
  // 导航栏国际化
  navbar: {
    dashboard: '首页',
    logout: '注销',
    document: '项目文档',
    gitee: '码云'
  },
  option: {
    enable: '启用',
    disabled: '禁用'
  },
  rules: {
    emailTip: '请输入正确的邮箱地址',
    mobileTip: '请输入正确的手机号码',
    usernameTip: '用户名不能为空'
  }
}
