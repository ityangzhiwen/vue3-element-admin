/**
 * @description 下拉框select的选项
 */
import i18n from '@/lang/index'
const t = (attr: string) => i18n.global.t(`option.${attr}`)

/**
 * 单个option对象
 */
interface optionData {
  /**
   * 值
   */
  value?: string | number
  /**
   * 标签
   */
  label?: string
}

/**
 * @description 公共函数用来回显文本
 */
export const getOptionText = (
  dataList: optionData[],
  value: string | number,
  defaultVal = ''
) => {
  const obj = dataList.find((item) => item.value === value)
  return obj ? obj.label : defaultVal
}

// 基本状态
export const statusOptions = () => {
  return [
    {
      value: 0,
      label: t('disabled')
    },
    {
      value: 1,
      label: t('enable')
    }
  ]
}
