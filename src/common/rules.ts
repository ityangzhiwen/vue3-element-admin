/**
 * @description form表单的校验规则
 */
import i18n from '@/lang/index'
const t = (attr: string) => i18n.global.t(`rules.${attr}`)

// 用户名
export const username = () => {
  return [
    {
      required: true,
      message: t('usernameTip'),
      trigger: 'blur'
    }
  ]
}

// 邮箱
export const email = () => {
  return [
    {
      required: false,
      pattern: /\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/,
      message: t('emailTip'),
      trigger: 'blur'
    }
  ]
}

// 手机号码
export const mobile = () => {
  return [
    {
      required: false,
      pattern: /^1[3|4|5|6|7|8|9][0-9]\d{8}$/,
      message: t('mobileTip'),
      trigger: 'blur'
    }
  ]
}
