import { loginApi, logoutApi } from '@/api/auth'
import { getUserInfoApi } from '@/api/user'
import { getDictTypePages } from '@/api/dict'
import { resetRouter } from '@/router'
import { store } from '@/store'

import { LoginData } from '@/api/auth/types'
import { UserInfo } from '@/api/user/types'

export const useUserStore = defineStore('user', () => {
  const user: UserInfo = {
    roles: [],
    perms: []
  }

  const token = useStorage('token', '')
  const dictList: any = ref([])

  /**
   * 登录
   *
   * @param {LoginData}
   * @returns
   */
  function login(loginData: LoginData) {
    return new Promise<void>((resolve, reject) => {
      loginApi(loginData)
        .then((response) => {
          const { tokenType, accessToken } = response.data
          token.value = tokenType + ' ' + accessToken // Bearer eyJhbGciOiJIUzI1NiJ9.xxx.xxx
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  // 获取信息(用户昵称、头像、角色集合、权限集合)
  function getUserInfo() {
    return new Promise<UserInfo>((resolve, reject) => {
      getUserInfoApi()
        .then(({ data }) => {
          if (!data) {
            reject('Verification failed, please Login again.')
            return
          }
          if (!data.roles || data.roles.length <= 0) {
            reject('getUserInfo: roles must be a non-null array!')
            return
          }
          Object.assign(user, { ...data })
          resolve(data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  // user logout
  function logout() {
    return new Promise<void>((resolve, reject) => {
      logoutApi()
        .then(() => {
          token.value = ''
          location.reload() // 清空路由
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  // remove token
  function resetToken() {
    console.log('resetToken')
    return new Promise<void>((resolve) => {
      token.value = ''
      resetRouter()
      resolve()
    })
  }

  // 获取字典数据
  function getDictList() {
    return new Promise<any>((resolve, reject) => {
      getDictTypePages()
        .then(({ data }) => {
          console.log('获取字典数据', data)
          dictList.value = data
          resolve(data)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  function getDictOptions(code: string) {
    for (const item of dictList.value) {
      if (item.code === code) {
        return item.children || []
      } else if (item.children && item.children.length) {
        return getDictOptions(code)
      }
    }
  }

  return {
    user,
    dictList,
    login,
    getUserInfo,
    logout,
    resetToken,
    getDictList,
    getDictOptions
  }
})

// 非setup
export function useUserStoreHook() {
  return useUserStore(store)
}
