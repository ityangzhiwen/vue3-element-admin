// uno.config.ts
import {
  defineConfig,
  presetAttributify,
  presetIcons,
  presetTypography,
  presetUno,
  presetWebFonts,
  transformerDirectives,
  transformerVariantGroup
} from 'unocss'
import presetRemToPx from '@unocss/preset-rem-to-px'

export default defineConfig({
  shortcuts: {
    'flex-center': 'flex justify-center items-center',
    'flex-x-center': 'flex justify-center',
    'flex-y-center': 'flex items-center',
    'wh-full': 'w-full h-full',
    'flex-x-between': 'flex items-center justify-between',
    'flex-x-end': 'flex items-center justify-end',
    'absolute-tl': 'absolute top-0 left-0',
    'absolute-tr': 'absolute top-0 right-0'
  },
  theme: {
    colors: {
      primary: 'var(--el-color-primary)',
      primary_dark: 'var(--el-color-primary-light-5)'
    }
  },
  presets: [
    presetUno(), //
    presetAttributify(), // 属性化模式
    presetIcons(), // 集成纯 CSS 图标
    presetTypography(),
    presetWebFonts({
      fonts: {
        // ...
      }
    }),
    presetRemToPx({
      baseFontSize: 16
    })
  ],
  transformers: [
    transformerDirectives(), // 快捷方式和指令
    transformerVariantGroup() // 前缀组，解决繁琐的多次写前缀的情况
  ]
})
